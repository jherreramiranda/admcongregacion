﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WindowsFormsApp1.Modelo
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BaseDatosCongregacionEntities : DbContext
    {
        public BaseDatosCongregacionEntities()
            : base("name=BaseDatosCongregacionEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<DetallePrecursores> DetallePrecursores { get; set; }
        public virtual DbSet<EstadoEspiritual> EstadoEspiritual { get; set; }
        public virtual DbSet<Grupos> Grupos { get; set; }
        public virtual DbSet<Publicadores> Publicadores { get; set; }
        public virtual DbSet<Actividad> Actividad { get; set; }
    }
}
