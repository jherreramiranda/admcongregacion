﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Modelo;

namespace WindowsFormsApp1.Controlador
{
    internal class PublicadorController
    {

        public IList cargarDataGrid() {
            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities()) { 
                var resultado = ( from a in contexto.Publicadores
                                  join b in contexto.EstadoEspiritual on a.EstadoEspiritual equals b.idEstado
                                  join c in contexto.Grupos on a.GSC equals c.idGrupo
                                  orderby a.nombres ascending
                                  select new 
                                  { 
                                      ID = a.id,
                                      Nombres = a.nombres,
                                      Paterno = a.ApellidoPaterno,
                                      Materno = a.ApellidoMaterno,
                                      Casada = a.ApellidoCasada,
                                      Genero = a.Genero,
                                      FechaNacimiento = a.FechaNacimiento,
                                      Direccion = a.Direccion,
                                      Telefono = a.Telefono,
                                      Celular = a.Celular,
                                      Email = a.Email,
                                      ContactoEmergencia = a.ContactoEmergencia,
                                      FechaBautismo = a.FechaBautismo,
                                      FechaPNB = a.FechaPNB,
                                      TipoRebano = a.TipoRebaño.HasValue ? "Otras Ovejas" : "Ungido",
                                      EstadoEspiritual = b.NombreEstado,
                                      GSC = c.NombreGrupo,
                                      ANC = a.Anciano,
                                      SMinisterial = a.SMinisterial,
                                      Precursor = a.Precursor,
                                      PNB = a.PNB,
                                      IdEstadoEspiritual = a.EstadoEspiritual,
                                      IdGrupoCongregacion = a.GSC

                                  }
                                ).ToList();
                return resultado;
            }
        }


        public IList obtenerDetallePrecursor(int idPublicador) {
            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                var resultado = (from a in contexto.DetallePrecursores
                                 where a.idPublicador == idPublicador
                                 select new { 
                                    TipoPrecursor = a.Sigla,
                                    Fecha = a.FechaInicio,
                                    IsIndefinido = a.Indefinido
                                 
                                 }).ToList();
               
                return resultado;
            }
        }

        private string sigla;
        private DateTime FechaInicioPrecursorado;
        private bool indefinido;

        public void agregarPublicador(string nombresPublicador, string apPaterno, string apMaterno, string apCasada, string genero, DateTime fechaNacimiento, 
                                      string direccion, string telefono, string celular, string email, string contactoEmergencia,
                                      int tipoRebano, int estadoEspiritual, DateTime fechaBautismo, int grupo, bool precursor, bool sMinisterial, bool anciano, bool pnb, 
                                      bool pRegular, DateTime fechaInicioRegular, bool pAuxiliar, DateTime fechaInicioAuxiliar, bool isIndefinido, DateTime fechaPNB) {

         

            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities()) 
            {
                using (var dbContextDetalleTransaction = contexto.Database.BeginTransaction())
                {
                    try
                    {
                        Publicadores publicadores = new Publicadores
                        {
                            nombres = nombresPublicador,
                            ApellidoPaterno = apPaterno,
                            ApellidoMaterno = apMaterno,
                            ApellidoCasada = apCasada,
                            Genero = genero,
                            FechaNacimiento = fechaNacimiento,
                            Direccion = direccion,
                            Telefono = telefono,
                            Celular = celular,
                            Email = email,
                            ContactoEmergencia = contactoEmergencia,
                            TipoRebaño = tipoRebano,
                            EstadoEspiritual = estadoEspiritual,
                            FechaBautismo = fechaBautismo,
                            FechaPNB = fechaPNB,
                            GSC = grupo,
                            Precursor = precursor,
                            SMinisterial = sMinisterial,
                            Anciano = anciano,
                            PNB = pnb

                        };

                        contexto.Publicadores.Add(publicadores);
                        contexto.SaveChanges();

                        

                        if (precursor) {

                            if (pRegular)
                            {
                                sigla = "PR";
                                FechaInicioPrecursorado = fechaInicioRegular;
                                indefinido = true;
                            }
                            else if (pAuxiliar) {
                                sigla = "PA";
                                FechaInicioPrecursorado = fechaInicioAuxiliar;
                                indefinido = isIndefinido;
                            }

                            //TODO: MEJORAR POR METODO "CREAR DETALLE PRECURSOR"
                            DetallePrecursores detallePrecursor = new DetallePrecursores 
                            {
                                idPublicador = publicadores.id,
                                Sigla = sigla,
                                FechaInicio = FechaInicioPrecursorado,
                                Indefinido = indefinido,
                            };
                                               
                            contexto.DetallePrecursores.Add(detallePrecursor);
                            contexto.SaveChanges();
                        }

                        dbContextDetalleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextDetalleTransaction.Rollback();
                    }
                
                }
                    
            }
        }


        public void modificarPublicador(int idPublicador, string nombresPublicador, string apPaterno, string apMaterno, string apCasada, string genero, DateTime fechaNacimiento,
                                      string direccion, string telefono, string celular, string email, string contactoEmergencia,
                                      int tipoRebano, int estadoEspiritual, DateTime fechaBautismo, int grupo, bool precursor, bool sMinisterial, bool anciano, bool pnb,
                                      bool pRegular, DateTime fechaInicioRegular, bool pAuxiliar, DateTime fechaInicioAuxiliar, bool isIndefinido, DateTime fechaPNB)
        {
            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                using (var dbContextDetalleTransaction = contexto.Database.BeginTransaction())
                {

                    Publicadores publ = contexto.Publicadores.FirstOrDefault(x => x.id == idPublicador); //obtengo publicador a modificar

                    try
                    {
                        publ.nombres = nombresPublicador;
                        publ.ApellidoPaterno = apPaterno;
                        publ.ApellidoMaterno = apMaterno;
                        publ.ApellidoCasada = apCasada;
                        publ.Genero = genero;
                        publ.FechaNacimiento = fechaNacimiento;
                        publ.Direccion = direccion;
                        publ.Telefono = telefono;
                        publ.Celular = celular;
                        publ.Email = email;
                        publ.ContactoEmergencia = contactoEmergencia;
                        publ.TipoRebaño = tipoRebano;
                        publ.EstadoEspiritual = estadoEspiritual;
                        publ.FechaBautismo = fechaBautismo;
                        publ.FechaPNB = fechaPNB;
                        publ.GSC = grupo;
                        publ.Precursor = precursor;
                        publ.SMinisterial = sMinisterial;
                        publ.Anciano = anciano;
                        publ.PNB = pnb;

                        contexto.SaveChanges();

                        
                        if (precursor)
                        {
                            if (pRegular)
                            {
                                sigla = "PR";
                                FechaInicioPrecursorado = fechaInicioRegular;
                                indefinido = true;
                            }
                            else if (pAuxiliar)
                            {
                                sigla = "PA";
                                FechaInicioPrecursorado = fechaInicioAuxiliar;
                                indefinido = isIndefinido;
                            }

                            bool existeDetallePrecursor = contexto.DetallePrecursores.Any(x => x.idPublicador == idPublicador);
                            if (existeDetallePrecursor)
                            {
                                DetallePrecursores detallePrecursor = contexto.DetallePrecursores.FirstOrDefault(x => x.idPublicador == idPublicador); //obtengo DetallePublicador del publicador que quiero modificar
                                detallePrecursor.Sigla = sigla;
                                detallePrecursor.FechaInicio = FechaInicioPrecursorado;
                                detallePrecursor.Indefinido = indefinido;
                            }
                            else 
                            {
                                //TODO: MEJORAR POR METODO "CREAR DETALLE PRECURSOR"
                                DetallePrecursores detallePrecursor = new DetallePrecursores
                                {
                                    idPublicador = idPublicador,
                                    Sigla = sigla,
                                    FechaInicio = FechaInicioPrecursorado,
                                    Indefinido = indefinido,
                                };

                                contexto.DetallePrecursores.Add(detallePrecursor);
                            }

                            contexto.SaveChanges();
                        }
                        else
                        {
                            eliminarDetallePrecursor(idPublicador);
                        }

                        dbContextDetalleTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextDetalleTransaction.Rollback();
                    }
                }
            }
        }


        public void eliminarPublicador(int idPublicador)
        {

            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                using (var dbContextDetalleTransaction = contexto.Database.BeginTransaction())
                {
                    bool existeDetallePrecursor = contexto.DetallePrecursores.Any(x => x.idPublicador == idPublicador);
                    try
                    {
                        if (existeDetallePrecursor)
                        {
                            eliminarDetallePrecursor(idPublicador);
                        }


                        Publicadores publ = contexto.Publicadores.FirstOrDefault(x => x.id == idPublicador); //obtengo publicador a eliminar
                        contexto.Publicadores.Remove(publ);

                        contexto.SaveChanges();
                        dbContextDetalleTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        dbContextDetalleTransaction.Rollback();
                    }
                }
            }
        }

        public void eliminarDetallePrecursor(int idPublicador) 
        {
            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                DetallePrecursores detallePrecursor = contexto.DetallePrecursores.FirstOrDefault(x => x.idPublicador == idPublicador); //obtengo detalle precursor a eliminar
                contexto.DetallePrecursores.Remove(detallePrecursor);

                contexto.SaveChanges();
            }
        }


        public IList busquedaPublicadoresPorNombreOrGrupo(string nombrePublicador) {
            
            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                var resultado = (from a in contexto.Publicadores
                                    join b in contexto.EstadoEspiritual on a.EstadoEspiritual equals b.idEstado
                                    join c in contexto.Grupos on a.GSC equals c.idGrupo
                                    where a.nombres.Contains(nombrePublicador)
                                    orderby a.nombres ascending
                                    select new
                                    {
                                        ID = a.id,
                                        Nombres = a.nombres,
                                        Paterno = a.ApellidoPaterno,
                                        Materno = a.ApellidoMaterno,
                                        Casada = a.ApellidoCasada,
                                        Genero = a.Genero,
                                        FechaNacimiento = a.FechaNacimiento,
                                        Direccion = a.Direccion,
                                        Telefono = a.Telefono,
                                        Celular = a.Celular,
                                        Email = a.Email,
                                        ContactoEmergencia = a.ContactoEmergencia,
                                        FechaBautismo = a.FechaBautismo,
                                        FechaPNB = a.FechaPNB,
                                        TipoRebano = a.TipoRebaño.HasValue ? "Otras Ovejas" : "Ungido",
                                        EstadoEspiritual = b.NombreEstado,
                                        GSC = c.NombreGrupo,
                                        ANC = a.Anciano,
                                        SMinisterial = a.SMinisterial,
                                        Precursor = a.Precursor,
                                        PNB = a.PNB,
                                        IdEstadoEspiritual = a.EstadoEspiritual,
                                        IdGrupoCongregacion = a.GSC
                                    }).ToList();
                return resultado;
            }
        }


        public IList obtenerTodosPublicadores() {

            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities()) {

                var resultado = (from a in contexto.Publicadores
                                 where a.EstadoEspiritual == 100 || a.EstadoEspiritual == 101
                                 select a).ToList(); //obtiene todos los publicadores  activos e inactivos


                return resultado;
            }
        }
    }
}
