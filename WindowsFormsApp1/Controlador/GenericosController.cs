﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Modelo;
using System.Globalization;

namespace WindowsFormsApp1.Controlador
{
    internal class GenericosController
    {

        public IList obtenerEstadoEspiritual()
        {

            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                var resultado = (from a in contexto.EstadoEspiritual
                                 select new
                                 {
                                     IdEstadoEspiritual = a.idEstado,
                                     NombreEstadoEspiritual = a.NombreEstado
                                 }).ToList();

                return resultado;
            }
        }


        public IList obtenerGruposCongregacion()
        {
            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                var resultado = (from a in contexto.Grupos
                                 select new
                                 {
                                     IdGrupo = a.idGrupo,
                                     NombreGrupo = a.NombreGrupo
                                 }).ToList();

                return resultado;
            }
        }


        public string obtenerNombreMesNumero(int numeroMes)
        {
            try
            {
                DateTimeFormatInfo formatoFecha = CultureInfo.CurrentCulture.DateTimeFormat;
                string nombreMes = formatoFecha.GetMonthName(numeroMes);
                return nombreMes;
            }
            catch
            {
                return "Desconocido";
            }
        }

    }

}
