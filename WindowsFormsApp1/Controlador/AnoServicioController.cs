﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Modelo;

namespace WindowsFormsApp1.Controlador
{
    internal class AnoServicioController
    {

        public bool CrearAnoServicioFull() {       
            /*
                1.- obtener fecha inicio año servicio
                2.- obtener cantidad total de publicadores
                3.- crear registros desde sept a agosto en tabla Actividad por cada publicador obtenido
            */

            //iniciacion de elementos necesarios
            DateTime fechaEjecucion = DateTime.Today;
            DateTime fechaActual = new DateTime(DateTime.Now.Year, 9, 1); // siempre empezar ano servicio en septiembre
            DateTime fechaFinal = fechaActual.AddMonths(11); // determinando la fecha final

            PublicadorController publicador = new PublicadorController();
            IList<Publicadores> publicadores = (IList<Publicadores>)publicador.obtenerTodosPublicadores();

            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                using (var dbContextDetalleTransaction = contexto.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var item in publicadores)
                        {

                            if (!isExistsAnoServicioPublicador(item.id, fechaActual.Year)) {

                                while (fechaActual <= fechaFinal)
                                {
                                    Actividad anoServicio = new Actividad
                                    {
                                        mes = fechaActual.Month,
                                        nombreMes = new GenericosController().obtenerNombreMesNumero(fechaActual.Month),
                                        anio = fechaActual.Year,
                                        idPublicador = item.id,
                                        publicaciones = 0,
                                        videos = 0,
                                        horas = 0,
                                        revisitas = 0,
                                        estudios = 0,
                                        observaciones = "",
                                        fechaUltimaModificacion = fechaEjecucion,
                                        fechaCreacion = fechaEjecucion
                                    };

                                    contexto.Actividad.Add(anoServicio);
                                    fechaActual = fechaActual.AddMonths(1);
                                }

                                fechaActual = new DateTime(DateTime.Now.Year, 9, 1); // reseteo fecha actual

                            }
                            else {
                                continue;
                            }
                        }
                        contexto.SaveChanges();
                        dbContextDetalleTransaction.Commit();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbContextDetalleTransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        private bool isExistsAnoServicioPublicador(int idPublicador, int anoInicial)
        {
            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                var resultado = contexto.Actividad.Any(x => x.idPublicador == idPublicador && (x.anio == anoInicial || x.anio == anoInicial + 1));
                return resultado;

            }
        }


        public IList cargarDataGridActividad()
        {
            //TODO: agregar filtro de mes
            using (BaseDatosCongregacionEntities contexto = new BaseDatosCongregacionEntities())
            {
                var resultado = (from a in contexto.Actividad
                                 join p in contexto.Publicadores on a.idPublicador equals p.id
                                 join g in contexto.Grupos on p.GSC equals g.idGrupo
                                 select new
                                 {
                                     PUBLICADOR = p.nombres+" "+p.ApellidoPaterno +" "+p.ApellidoMaterno,
                                     GRUPO = g.NombreGrupo,
                                     IDGRUPO = p.GSC,
                                     MES = a.nombreMes,
                                     AÑO = a.anio,
                                     PUBLICACIONES = a.publicaciones,
                                     VIDEOS = a.videos,
                                     HORAS = a.horas,
                                     REVISITAS = a.revisitas,
                                     ESTUDIOS = a.estudios,
                                     OBSERVACIONES = a.observaciones
                                 }
                                ).ToList();
                return resultado;
            }
        }
    }
}
