﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Controlador;

namespace WindowsFormsApp1.Vista
{
    public partial class Publicadores : Form
    {

        // PARA LA OBTENCION DE DATOS
        private string nombres, apellidoPaterno, apellidoMaterno, apellidoCasada, genero, direccion, telefono, celular, email, contactoEmergencia, tipoPrecursor, fechaInicio;
        private DateTime fechaNacimiento, fechaBautismo, fechaInicioPRegular, fechaInicioPAuxiliar, fechaInicioPNB;
        private int idPubl, tipoRebano, estadoEspiritual, grupo;



        private bool isPrecursor, isSMinisterial, isAnciano, isPNB, isPRegular, isPAuxiliar, isIndefinido, llenarCampoIndefinido;



        public Publicadores()
        {
            InitializeComponent();
        }

        private void bunifuTSwitchPrecursor_CheckedChanged(object sender, Bunifu.UI.WinForms.BunifuToggleSwitch.CheckedChangedEventArgs e)
        {
            bunifuRbtnAuxiliar.Checked = false;
            bunifuRbtnRegular.Checked = false;
            bunifuCkbPAIndefinido.Checked = false;

            bunifuDPFechaInicioRegular.Visible = false;
            bunifuDPFechaInicioAuxiliar.Visible = false;
            bunifuCkbPAIndefinido.Visible = false;
            bunifuLblPAIndefinido.Visible = false;

            if (bunifuTSwitchPrecursor.Checked)
            {
                bunifuLblRegular.Visible = true;
                bunifuLblAuxiliar.Visible = true;
                bunifuRbtnAuxiliar.Visible = true;
                bunifuRbtnRegular.Visible = true;
             

            }
            else {
                bunifuLblRegular.Visible = false;
                bunifuLblAuxiliar.Visible = false;
                bunifuRbtnAuxiliar.Visible = false;
                bunifuRbtnRegular.Visible = false;
            }
        }

        private void bunifuRbtnRegular_CheckedChanged2(object sender, Bunifu.UI.WinForms.BunifuRadioButton.CheckedChangedEventArgs e)
        {
            if (bunifuRbtnRegular.Checked)
            {
                bunifuDPFechaInicioRegular.Visible = true;
                
                bunifuDPFechaInicioAuxiliar.Visible = false;
                bunifuCkbPAIndefinido.Visible = false;
                bunifuLblPAIndefinido.Visible = false;
                bunifuCkbPAIndefinido.Checked = false;

            }
            else {
                bunifuDPFechaInicioRegular.Visible = false;
            }
        }

        private void bunifuRbtnAuxiliar_CheckedChanged2(object sender, Bunifu.UI.WinForms.BunifuRadioButton.CheckedChangedEventArgs e)
        {
            bunifuDPFechaInicioRegular.Visible = false;
            

            if (bunifuRbtnAuxiliar.Checked)
            {
                bunifuDPFechaInicioAuxiliar.Visible = true;
                bunifuCkbPAIndefinido.Visible = true;
                bunifuLblPAIndefinido.Visible = true;
            }
            else {
                bunifuCkbPAIndefinido.Visible = false;
                bunifuLblPAIndefinido.Visible = false;
            }
        }


        private void bunifuBtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bunifuTSwitchPNB_CheckedChanged(object sender, Bunifu.UI.WinForms.BunifuToggleSwitch.CheckedChangedEventArgs e)
        {
            if (bunifuTSwitchPNB.Checked)
            {
                bunifuLblFechaInicioPNB.Visible = true;
                bunifuDPFechaInicioPNB.Visible = true;

                bunifuDPFechaBautismo.Enabled = false;
                bunifuDPFechaBautismo.Value = DateTime.Parse("01-01-1900");
            }
            else
            {
                bunifuLblFechaInicioPNB.Visible = false;
                bunifuDPFechaInicioPNB.Visible = false;
                bunifuDPFechaInicioPNB.Value = DateTime.Parse("01-01-1900");

                bunifuDPFechaBautismo.Enabled = true;
                bunifuDPFechaBautismo.Value = DateTime.Now;
            }
            
        }

        private void Publicadores_Load(object sender, EventArgs e)
        {
            PublicadorController publicador = new PublicadorController();
            GenericosController controlesGenericos = new GenericosController();
            
            bunifuDataGridPublicadores.DataSource = publicador.cargarDataGrid();


            //ocultando algunas celdas
            bunifuDataGridPublicadores.Columns[10].Visible = false; // Email
            bunifuDataGridPublicadores.Columns[11].Visible = false; // ContactoEmergencia
            bunifuDataGridPublicadores.Columns[12].Visible = false; // FechaBautismo
            bunifuDataGridPublicadores.Columns[13].Visible = false; // FechaPNB
            bunifuDataGridPublicadores.Columns[14].Visible = false; // TipoRebano
            bunifuDataGridPublicadores.Columns[17].Visible = false; // Anciano
            bunifuDataGridPublicadores.Columns[18].Visible = false; // Siervo Ministerial
            bunifuDataGridPublicadores.Columns[19].Visible = false; // Precursor
            bunifuDataGridPublicadores.Columns[20].Visible = false; // PNB
            bunifuDataGridPublicadores.Columns[21].Visible = false; // IDEstadoEspiritual
            bunifuDataGridPublicadores.Columns[22].Visible = false; // IDGrupos

            var listaEstadoEspiritual = controlesGenericos.obtenerEstadoEspiritual();
            var listaGruposCogregacion = controlesGenericos.obtenerGruposCongregacion();


            this.bunifuCbxEstadoEspiritual.DataSource = listaEstadoEspiritual;
            this.bunifuCbxEstadoEspiritual.ValueMember = "IdEstadoEspiritual";
            this.bunifuCbxEstadoEspiritual.DisplayMember = "NombreEstadoEspiritual";



            this.bunifuCbxGSC.DataSource = listaGruposCogregacion;
            this.bunifuCbxGSC.ValueMember = "IdGrupo";
            this.bunifuCbxGSC.DisplayMember = "NombreGrupo";

        }

        private void llenarCampos() { 
        
            //CARGA DE SECCION - DATOS PERSONALES
            string idPublicador = bunifuDataGridPublicadores.SelectedRows[0].Cells[0].Value.ToString();

            this.bunifuTxtIdPublicador.Text = idPublicador;
            this.bunifuTxtNombres.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[1].Value.ToString();
            this.bunifuTxtApPaterno.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[2].Value.ToString();
            this.bunifuTxtApMaterno.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[3].Value.ToString();
            this.bunifuTxtApCasada.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[4].Value is null ? "" : bunifuDataGridPublicadores.SelectedRows[0].Cells[4].Value.ToString();
            this.bunifuCbxGenero.SelectedIndex = bunifuDataGridPublicadores.SelectedRows[0].Cells[5].Value.ToString() == "H" ? 0 : 1;
            this.bunifuDPFechaNacimiento.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[6].Value.ToString();

            //CARGA DE SECCION - DATOS CONTACTO
            this.bunifuTxtDireccion.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[7].Value is null ? "" : bunifuDataGridPublicadores.SelectedRows[0].Cells[7].Value.ToString();
            this.bunifuTxtTelefono.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[8].Value is null ? "" : bunifuDataGridPublicadores.SelectedRows[0].Cells[8].Value.ToString();
            this.bunifuTxtCelular.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[9].Value is null ? "" : bunifuDataGridPublicadores.SelectedRows[0].Cells[9].Value.ToString();
            this.bunifuTxtEmail.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[10].Value is null ? "" : bunifuDataGridPublicadores.SelectedRows[0].Cells[10].Value.ToString();
            this.bunifuTxtContactoEmergencia.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[11].Value is null ? "" : bunifuDataGridPublicadores.SelectedRows[0].Cells[11].Value.ToString();

            //CARGA DE SECCION - ANTECEDENTES ESPIRITUALES
            this.bunifuDPFechaBautismo.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[12].Value.ToString();
            this.bunifuDPFechaInicioPNB.Text = bunifuDataGridPublicadores.SelectedRows[0].Cells[13].Value is null ? "" : bunifuDataGridPublicadores.SelectedRows[0].Cells[13].Value.ToString(); ;
            this.bunifuCbxTipoRebano.SelectedIndex = bunifuDataGridPublicadores.SelectedRows[0].Cells[14].Value.ToString() == "Otras Ovejas" ? 0 : 1;

            this.bunifuTSwitchAnciano.Checked = (bool)bunifuDataGridPublicadores.SelectedRows[0].Cells[17].Value;
            this.bunifuTSwitchMinisterial.Checked = (bool)bunifuDataGridPublicadores.SelectedRows[0].Cells[18].Value;
            this.bunifuTSwitchPrecursor.Checked = (bool)bunifuDataGridPublicadores.SelectedRows[0].Cells[19].Value;
            this.bunifuTSwitchPNB.Checked = (bool)bunifuDataGridPublicadores.SelectedRows[0].Cells[20].Value;

            this.bunifuCbxEstadoEspiritual.SelectedValue = int.Parse(bunifuDataGridPublicadores.SelectedRows[0].Cells[21].Value.ToString());
            this.bunifuCbxGSC.SelectedValue = int.Parse(bunifuDataGridPublicadores.SelectedRows[0].Cells[22].Value.ToString());

            //Llenando tipo Precursor
            if (bunifuTSwitchPrecursor.Checked) {

                PublicadorController precursor = new PublicadorController();
                IList detallePrecursor = precursor.obtenerDetallePrecursor(int.Parse(idPublicador));

                if (detallePrecursor.Count > 0) 
                {
                    tipoPrecursor = detallePrecursor[0].GetType().GetProperty("TipoPrecursor").GetValue(detallePrecursor[0], null).ToString();
                    fechaInicio = detallePrecursor[0].GetType().GetProperty("Fecha").GetValue(detallePrecursor[0], null).ToString();
                    llenarCampoIndefinido = (bool)detallePrecursor[0].GetType().GetProperty("IsIndefinido").GetValue(detallePrecursor[0], null);
                }
                             

                if (tipoPrecursor == "PR") {
                    this.bunifuRbtnRegular.Checked = true;
                    this.bunifuDPFechaInicioRegular.Text = fechaInicio;
                }
                if (tipoPrecursor == "PA") {
                    this.bunifuRbtnAuxiliar.Checked = true;
                    this.bunifuDPFechaInicioAuxiliar.Text = fechaInicio;
                    this.bunifuCkbPAIndefinido.Checked = llenarCampoIndefinido;
                }
            }

        }

        private void bunifuDataGridPublicadores_Click(object sender, EventArgs e)
        {
            llenarCampos();
        }


        private void obtenerDatosPublicadores() {

            // DATOS PERSONALES
            idPubl = int.Parse(bunifuTxtIdPublicador.Text != "" ? bunifuTxtIdPublicador.Text : "0");
            nombres = bunifuTxtNombres.Text;
            apellidoPaterno = bunifuTxtApPaterno.Text;
            apellidoMaterno = bunifuTxtApMaterno.Text;
            apellidoCasada = bunifuTxtApCasada.Text;
            genero = bunifuCbxGenero.Text;
            fechaNacimiento = bunifuDPFechaNacimiento.Value;


            // DATOS CONTACTO
            direccion = bunifuTxtDireccion.Text;
            telefono = bunifuTxtTelefono.Text;
            celular = bunifuTxtCelular.Text;
            email = bunifuTxtEmail.Text;
            contactoEmergencia = bunifuTxtContactoEmergencia.Text;

            // DATOS ESPIRITUALES

            tipoRebano = bunifuCbxTipoRebano.Text == "Otras Ovejas" ? 1 : 0;
            estadoEspiritual = int.Parse(bunifuCbxEstadoEspiritual.SelectedValue.ToString());
            grupo = int.Parse(bunifuCbxGSC.SelectedValue.ToString());
            fechaBautismo = bunifuDPFechaBautismo.Value;
            isPrecursor = bunifuTSwitchPrecursor.Value;
            isSMinisterial = bunifuTSwitchMinisterial.Value;
            isAnciano = bunifuTSwitchAnciano.Value;

            isPNB = bunifuTSwitchPNB.Value;

            fechaInicioPNB = bunifuDPFechaInicioPNB.Value;

            isPRegular = bunifuRbtnRegular.Checked;
            fechaInicioPRegular = bunifuDPFechaInicioRegular.Value;

            isPAuxiliar = bunifuRbtnAuxiliar.Checked;
            fechaInicioPAuxiliar = bunifuDPFechaInicioAuxiliar.Value;

            isIndefinido = bunifuCkbPAIndefinido.Checked;
        }


        private void limpiarCamposPublicador() 
        {
            // DATOS PERSONALES
            bunifuTxtIdPublicador.Text = "";
            bunifuTxtNombres.Text = "";
            bunifuTxtApPaterno.Text = "";
            bunifuTxtApMaterno.Text = "";
            bunifuTxtApCasada.Text = "";
            bunifuCbxGenero.Text = "";
            bunifuDPFechaNacimiento.Value = DateTime.Parse("01-01-1900");


            // DATOS CONTACTO
            bunifuTxtDireccion.Text = "";
            bunifuTxtTelefono.Text = "";
            bunifuTxtCelular.Text = "";
            bunifuTxtEmail.Text = "";
            bunifuTxtContactoEmergencia.Text = "";

            // DATOS ESPIRITUALES

            bunifuCbxTipoRebano.Text = "";
            bunifuCbxEstadoEspiritual.Text = "";
            bunifuCbxGSC.Text = "";
            bunifuDPFechaBautismo.Value = DateTime.Parse("01-01-1900");
            bunifuTSwitchPrecursor.Value = false;
            bunifuTSwitchMinisterial.Value = false;
            bunifuTSwitchAnciano.Value = false;

            bunifuTSwitchPNB.Value = false;

            bunifuDPFechaInicioPNB.Value = DateTime.Parse("01-01-1900");

            bunifuRbtnRegular.Checked = false;
            bunifuDPFechaInicioRegular.Value = DateTime.Parse("01-01-1900");

            bunifuRbtnAuxiliar.Checked = false;
            bunifuDPFechaInicioAuxiliar.Value = DateTime.Parse("01-01-1900");

            bunifuCkbPAIndefinido.Checked = false;

            PublicadorController publicador = new PublicadorController();
            bunifuDataGridPublicadores.DataSource = publicador.cargarDataGrid();
        }

        private void bunifuBtnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarCamposPublicador();
        }


        private void bunifuBtnAgregar_Click(object sender, EventArgs e)
        {

            obtenerDatosPublicadores();

            if (idPubl != 0) {
                MessageBox.Show("Publicador ya existe, intente modificarlo","Informacion",MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                PublicadorController publicador = new PublicadorController();

                publicador.agregarPublicador(nombres, apellidoPaterno, apellidoMaterno, apellidoCasada, genero, fechaNacimiento,
                direccion, telefono, celular, email, contactoEmergencia,
                tipoRebano, estadoEspiritual, fechaBautismo, grupo,
                isPrecursor, isSMinisterial, isAnciano, isPNB, isPRegular, fechaInicioPRegular, isPAuxiliar, fechaInicioPAuxiliar, isIndefinido, fechaInicioPNB);

                limpiarCamposPublicador();

                MessageBox.Show("Publicador Agregado Correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bunifuBtnEliminar_Click(object sender, EventArgs e)
        {
            obtenerDatosPublicadores();

            if (idPubl != 0)
            {
                PublicadorController publicador = new PublicadorController();
                publicador.eliminarPublicador(idPubl);
                limpiarCamposPublicador();
                MessageBox.Show("Publicador Eliminado Correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                MessageBox.Show("Debe Seleccionar Publicador a Eliminar", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } 

        }


        private void bunifuBtnBuscar_Click(object sender, EventArgs e)
        {
            string publicadorBuscado;

            publicadorBuscado = bunifuTxtNombreABuscar.Text;
           
            PublicadorController publicador = new PublicadorController();
            bunifuDataGridPublicadores.DataSource = publicador.busquedaPublicadoresPorNombreOrGrupo(publicadorBuscado);
            
        }

        private void bunifuBtnModificar_Click(object sender, EventArgs e)
        {
            obtenerDatosPublicadores();

            PublicadorController publicador = new PublicadorController();
            publicador.modificarPublicador(idPubl, nombres, apellidoPaterno, apellidoMaterno, apellidoCasada, genero, fechaNacimiento,
                direccion, telefono, celular, email, contactoEmergencia,
                tipoRebano, estadoEspiritual, fechaBautismo, grupo,
                isPrecursor, isSMinisterial, isAnciano, isPNB, isPRegular, fechaInicioPRegular, isPAuxiliar, fechaInicioPAuxiliar, isIndefinido, fechaInicioPNB);

            bunifuDataGridPublicadores.DataSource = publicador.cargarDataGrid();
        }
    }
}
