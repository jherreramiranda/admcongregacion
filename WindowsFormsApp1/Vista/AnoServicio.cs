﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Controlador;

namespace WindowsFormsApp1.Vista
{
    public partial class AnoServicio : Form
    {
        public AnoServicio()
        {
            InitializeComponent();
        }

        private void bunifuBtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bunifuBtnCrearAnoServicio_Click(object sender, EventArgs e)
        {
            AnoServicioController anioServcio = new AnoServicioController();
            if (anioServcio.CrearAnoServicioFull()) {
                MessageBox.Show("Año de Servicio Creado Correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else{
                MessageBox.Show("Error al crear año Servicio", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
