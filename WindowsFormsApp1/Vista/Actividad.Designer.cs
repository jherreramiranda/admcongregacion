﻿namespace WindowsFormsApp1.Vista
{
    partial class Actividad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Actividad));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderEdges();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderEdges borderEdges2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderEdges();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuLblGrupoServicio = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuBtnBuscar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton2();
            this.bunifuCbxGruposServicio = new Bunifu.UI.WinForms.BunifuDropdown();
            this.bunifuDataGridViewActividad = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.bunifuSeparator2 = new Bunifu.UI.WinForms.BunifuSeparator();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuBtnSalir = new Bunifu.UI.WinForms.BunifuButton.BunifuButton2();
            this.bunifuCards1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuDataGridViewActividad)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuCards1.Controls.Add(this.bunifuLblGrupoServicio);
            this.bunifuCards1.Controls.Add(this.bunifuBtnBuscar);
            this.bunifuCards1.Controls.Add(this.bunifuCbxGruposServicio);
            this.bunifuCards1.Controls.Add(this.bunifuDataGridViewActividad);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(37, 83);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(1289, 676);
            this.bunifuCards1.TabIndex = 0;
            // 
            // bunifuLblGrupoServicio
            // 
            this.bunifuLblGrupoServicio.AllowParentOverrides = false;
            this.bunifuLblGrupoServicio.AutoEllipsis = false;
            this.bunifuLblGrupoServicio.CursorType = null;
            this.bunifuLblGrupoServicio.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLblGrupoServicio.ForeColor = System.Drawing.Color.DimGray;
            this.bunifuLblGrupoServicio.Location = new System.Drawing.Point(15, 71);
            this.bunifuLblGrupoServicio.Name = "bunifuLblGrupoServicio";
            this.bunifuLblGrupoServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLblGrupoServicio.Size = new System.Drawing.Size(89, 17);
            this.bunifuLblGrupoServicio.TabIndex = 30;
            this.bunifuLblGrupoServicio.Text = "Grupo Servicio";
            this.bunifuLblGrupoServicio.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLblGrupoServicio.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuBtnBuscar
            // 
            this.bunifuBtnBuscar.AllowAnimations = true;
            this.bunifuBtnBuscar.AllowMouseEffects = true;
            this.bunifuBtnBuscar.AllowToggling = false;
            this.bunifuBtnBuscar.AnimationSpeed = 200;
            this.bunifuBtnBuscar.AutoGenerateColors = false;
            this.bunifuBtnBuscar.AutoRoundBorders = false;
            this.bunifuBtnBuscar.AutoSizeLeftIcon = true;
            this.bunifuBtnBuscar.AutoSizeRightIcon = true;
            this.bunifuBtnBuscar.BackColor = System.Drawing.Color.Transparent;
            this.bunifuBtnBuscar.BackColor1 = System.Drawing.Color.White;
            this.bunifuBtnBuscar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnBuscar.BackgroundImage")));
            this.bunifuBtnBuscar.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnBuscar.ButtonText = "       Buscar";
            this.bunifuBtnBuscar.ButtonTextMarginLeft = 0;
            this.bunifuBtnBuscar.ColorContrastOnClick = 45;
            this.bunifuBtnBuscar.ColorContrastOnHover = 45;
            this.bunifuBtnBuscar.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.bunifuBtnBuscar.CustomizableEdges = borderEdges1;
            this.bunifuBtnBuscar.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuBtnBuscar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuBtnBuscar.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuBtnBuscar.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuBtnBuscar.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.ButtonStates.Pressed;
            this.bunifuBtnBuscar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bunifuBtnBuscar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuBtnBuscar.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuBtnBuscar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuBtnBuscar.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuBtnBuscar.IconMarginLeft = 11;
            this.bunifuBtnBuscar.IconPadding = 10;
            this.bunifuBtnBuscar.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuBtnBuscar.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuBtnBuscar.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuBtnBuscar.IconSize = 25;
            this.bunifuBtnBuscar.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuBtnBuscar.IdleBorderRadius = 20;
            this.bunifuBtnBuscar.IdleBorderThickness = 3;
            this.bunifuBtnBuscar.IdleFillColor = System.Drawing.Color.White;
            this.bunifuBtnBuscar.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnBuscar.IdleIconLeftImage")));
            this.bunifuBtnBuscar.IdleIconRightImage = null;
            this.bunifuBtnBuscar.IndicateFocus = false;
            this.bunifuBtnBuscar.Location = new System.Drawing.Point(346, 94);
            this.bunifuBtnBuscar.Name = "bunifuBtnBuscar";
            this.bunifuBtnBuscar.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuBtnBuscar.OnDisabledState.BorderRadius = 20;
            this.bunifuBtnBuscar.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnBuscar.OnDisabledState.BorderThickness = 3;
            this.bunifuBtnBuscar.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuBtnBuscar.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuBtnBuscar.OnDisabledState.IconLeftImage = null;
            this.bunifuBtnBuscar.OnDisabledState.IconRightImage = null;
            this.bunifuBtnBuscar.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuBtnBuscar.onHoverState.BorderRadius = 20;
            this.bunifuBtnBuscar.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnBuscar.onHoverState.BorderThickness = 3;
            this.bunifuBtnBuscar.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuBtnBuscar.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnBuscar.onHoverState.IconLeftImage = null;
            this.bunifuBtnBuscar.onHoverState.IconRightImage = null;
            this.bunifuBtnBuscar.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuBtnBuscar.OnIdleState.BorderRadius = 20;
            this.bunifuBtnBuscar.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnBuscar.OnIdleState.BorderThickness = 3;
            this.bunifuBtnBuscar.OnIdleState.FillColor = System.Drawing.Color.White;
            this.bunifuBtnBuscar.OnIdleState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuBtnBuscar.OnIdleState.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnBuscar.OnIdleState.IconLeftImage")));
            this.bunifuBtnBuscar.OnIdleState.IconRightImage = null;
            this.bunifuBtnBuscar.OnPressedState.BorderColor = System.Drawing.Color.Green;
            this.bunifuBtnBuscar.OnPressedState.BorderRadius = 20;
            this.bunifuBtnBuscar.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnBuscar.OnPressedState.BorderThickness = 3;
            this.bunifuBtnBuscar.OnPressedState.FillColor = System.Drawing.Color.Green;
            this.bunifuBtnBuscar.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnBuscar.OnPressedState.IconLeftImage = null;
            this.bunifuBtnBuscar.OnPressedState.IconRightImage = null;
            this.bunifuBtnBuscar.Size = new System.Drawing.Size(113, 39);
            this.bunifuBtnBuscar.TabIndex = 29;
            this.bunifuBtnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuBtnBuscar.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuBtnBuscar.TextMarginLeft = 0;
            this.bunifuBtnBuscar.TextPadding = new System.Windows.Forms.Padding(0);
            this.bunifuBtnBuscar.UseDefaultRadiusAndThickness = true;
            // 
            // bunifuCbxGruposServicio
            // 
            this.bunifuCbxGruposServicio.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCbxGruposServicio.BackgroundColor = System.Drawing.Color.White;
            this.bunifuCbxGruposServicio.BorderColor = System.Drawing.Color.Silver;
            this.bunifuCbxGruposServicio.BorderRadius = 1;
            this.bunifuCbxGruposServicio.Color = System.Drawing.Color.Silver;
            this.bunifuCbxGruposServicio.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down;
            this.bunifuCbxGruposServicio.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.bunifuCbxGruposServicio.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuCbxGruposServicio.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.bunifuCbxGruposServicio.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.bunifuCbxGruposServicio.DisabledIndicatorColor = System.Drawing.Color.DarkGray;
            this.bunifuCbxGruposServicio.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.bunifuCbxGruposServicio.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thin;
            this.bunifuCbxGruposServicio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bunifuCbxGruposServicio.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left;
            this.bunifuCbxGruposServicio.FillDropDown = true;
            this.bunifuCbxGruposServicio.FillIndicator = false;
            this.bunifuCbxGruposServicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bunifuCbxGruposServicio.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bunifuCbxGruposServicio.ForeColor = System.Drawing.Color.Black;
            this.bunifuCbxGruposServicio.FormattingEnabled = true;
            this.bunifuCbxGruposServicio.Icon = null;
            this.bunifuCbxGruposServicio.IndicatorAlignment = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right;
            this.bunifuCbxGruposServicio.IndicatorColor = System.Drawing.Color.DarkGray;
            this.bunifuCbxGruposServicio.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right;
            this.bunifuCbxGruposServicio.IndicatorThickness = 2;
            this.bunifuCbxGruposServicio.IsDropdownOpened = false;
            this.bunifuCbxGruposServicio.ItemBackColor = System.Drawing.Color.White;
            this.bunifuCbxGruposServicio.ItemBorderColor = System.Drawing.Color.White;
            this.bunifuCbxGruposServicio.ItemForeColor = System.Drawing.Color.Black;
            this.bunifuCbxGruposServicio.ItemHeight = 26;
            this.bunifuCbxGruposServicio.ItemHighLightColor = System.Drawing.Color.DodgerBlue;
            this.bunifuCbxGruposServicio.ItemHighLightForeColor = System.Drawing.Color.White;
            this.bunifuCbxGruposServicio.ItemTopMargin = 3;
            this.bunifuCbxGruposServicio.Location = new System.Drawing.Point(15, 94);
            this.bunifuCbxGruposServicio.Name = "bunifuCbxGruposServicio";
            this.bunifuCbxGruposServicio.Size = new System.Drawing.Size(260, 32);
            this.bunifuCbxGruposServicio.TabIndex = 2;
            this.bunifuCbxGruposServicio.Text = null;
            this.bunifuCbxGruposServicio.TextAlignment = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left;
            this.bunifuCbxGruposServicio.TextLeftMargin = 5;
            // 
            // bunifuDataGridViewActividad
            // 
            this.bunifuDataGridViewActividad.AllowCustomTheming = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.bunifuDataGridViewActividad.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.bunifuDataGridViewActividad.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.bunifuDataGridViewActividad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bunifuDataGridViewActividad.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.bunifuDataGridViewActividad.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bunifuDataGridViewActividad.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.bunifuDataGridViewActividad.ColumnHeadersHeight = 40;
            this.bunifuDataGridViewActividad.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.bunifuDataGridViewActividad.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.bunifuDataGridViewActividad.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.bunifuDataGridViewActividad.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.bunifuDataGridViewActividad.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.bunifuDataGridViewActividad.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.bunifuDataGridViewActividad.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.bunifuDataGridViewActividad.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.bunifuDataGridViewActividad.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.bunifuDataGridViewActividad.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.bunifuDataGridViewActividad.CurrentTheme.HeaderStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(115)))), ((int)(((byte)(204)))));
            this.bunifuDataGridViewActividad.CurrentTheme.HeaderStyle.SelectionForeColor = System.Drawing.Color.White;
            this.bunifuDataGridViewActividad.CurrentTheme.Name = null;
            this.bunifuDataGridViewActividad.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.bunifuDataGridViewActividad.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.bunifuDataGridViewActividad.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.bunifuDataGridViewActividad.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.bunifuDataGridViewActividad.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.bunifuDataGridViewActividad.DefaultCellStyle = dataGridViewCellStyle3;
            this.bunifuDataGridViewActividad.EnableHeadersVisualStyles = false;
            this.bunifuDataGridViewActividad.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.bunifuDataGridViewActividad.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.bunifuDataGridViewActividad.HeaderBgColor = System.Drawing.Color.Empty;
            this.bunifuDataGridViewActividad.HeaderForeColor = System.Drawing.Color.White;
            this.bunifuDataGridViewActividad.Location = new System.Drawing.Point(15, 154);
            this.bunifuDataGridViewActividad.Name = "bunifuDataGridViewActividad";
            this.bunifuDataGridViewActividad.RowHeadersVisible = false;
            this.bunifuDataGridViewActividad.RowTemplate.Height = 40;
            this.bunifuDataGridViewActividad.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.bunifuDataGridViewActividad.Size = new System.Drawing.Size(1246, 509);
            this.bunifuDataGridViewActividad.TabIndex = 1;
            this.bunifuDataGridViewActividad.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuSeparator2.BackgroundImage")));
            this.bunifuSeparator2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuSeparator2.DashCap = Bunifu.UI.WinForms.BunifuSeparator.CapStyles.Flat;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.Silver;
            this.bunifuSeparator2.LineStyle = Bunifu.UI.WinForms.BunifuSeparator.LineStyles.Solid;
            this.bunifuSeparator2.LineThickness = 1;
            this.bunifuSeparator2.Location = new System.Drawing.Point(37, 39);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Orientation = Bunifu.UI.WinForms.BunifuSeparator.LineOrientation.Horizontal;
            this.bunifuSeparator2.Size = new System.Drawing.Size(1158, 30);
            this.bunifuSeparator2.TabIndex = 31;
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.AllowParentOverrides = false;
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel1.CursorType = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.ForeColor = System.Drawing.Color.SteelBlue;
            this.bunifuLabel1.Location = new System.Drawing.Point(37, 14);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(136, 19);
            this.bunifuLabel1.TabIndex = 32;
            this.bunifuLabel1.Text = "Ingreso Actividad";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuBtnSalir
            // 
            this.bunifuBtnSalir.AllowAnimations = true;
            this.bunifuBtnSalir.AllowMouseEffects = true;
            this.bunifuBtnSalir.AllowToggling = false;
            this.bunifuBtnSalir.AnimationSpeed = 200;
            this.bunifuBtnSalir.AutoGenerateColors = false;
            this.bunifuBtnSalir.AutoRoundBorders = false;
            this.bunifuBtnSalir.AutoSizeLeftIcon = true;
            this.bunifuBtnSalir.AutoSizeRightIcon = true;
            this.bunifuBtnSalir.BackColor = System.Drawing.Color.Transparent;
            this.bunifuBtnSalir.BackColor1 = System.Drawing.Color.White;
            this.bunifuBtnSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnSalir.BackgroundImage")));
            this.bunifuBtnSalir.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.ButtonText = "     Salir";
            this.bunifuBtnSalir.ButtonTextMarginLeft = 0;
            this.bunifuBtnSalir.ColorContrastOnClick = 45;
            this.bunifuBtnSalir.ColorContrastOnHover = 45;
            this.bunifuBtnSalir.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges2.BottomLeft = true;
            borderEdges2.BottomRight = true;
            borderEdges2.TopLeft = true;
            borderEdges2.TopRight = true;
            this.bunifuBtnSalir.CustomizableEdges = borderEdges2;
            this.bunifuBtnSalir.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuBtnSalir.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuBtnSalir.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuBtnSalir.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuBtnSalir.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.ButtonStates.Pressed;
            this.bunifuBtnSalir.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuBtnSalir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuBtnSalir.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuBtnSalir.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuBtnSalir.IconMarginLeft = 11;
            this.bunifuBtnSalir.IconPadding = 10;
            this.bunifuBtnSalir.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuBtnSalir.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuBtnSalir.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuBtnSalir.IconSize = 25;
            this.bunifuBtnSalir.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.IdleBorderRadius = 10;
            this.bunifuBtnSalir.IdleBorderThickness = 2;
            this.bunifuBtnSalir.IdleFillColor = System.Drawing.Color.White;
            this.bunifuBtnSalir.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnSalir.IdleIconLeftImage")));
            this.bunifuBtnSalir.IdleIconRightImage = null;
            this.bunifuBtnSalir.IndicateFocus = false;
            this.bunifuBtnSalir.Location = new System.Drawing.Point(1201, 26);
            this.bunifuBtnSalir.Name = "bunifuBtnSalir";
            this.bunifuBtnSalir.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuBtnSalir.OnDisabledState.BorderRadius = 10;
            this.bunifuBtnSalir.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.OnDisabledState.BorderThickness = 2;
            this.bunifuBtnSalir.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuBtnSalir.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuBtnSalir.OnDisabledState.IconLeftImage = null;
            this.bunifuBtnSalir.OnDisabledState.IconRightImage = null;
            this.bunifuBtnSalir.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.onHoverState.BorderRadius = 10;
            this.bunifuBtnSalir.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.onHoverState.BorderThickness = 2;
            this.bunifuBtnSalir.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnSalir.onHoverState.IconLeftImage = null;
            this.bunifuBtnSalir.onHoverState.IconRightImage = null;
            this.bunifuBtnSalir.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.OnIdleState.BorderRadius = 10;
            this.bunifuBtnSalir.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.OnIdleState.BorderThickness = 2;
            this.bunifuBtnSalir.OnIdleState.FillColor = System.Drawing.Color.White;
            this.bunifuBtnSalir.OnIdleState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.OnIdleState.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnSalir.OnIdleState.IconLeftImage")));
            this.bunifuBtnSalir.OnIdleState.IconRightImage = null;
            this.bunifuBtnSalir.OnPressedState.BorderColor = System.Drawing.Color.Maroon;
            this.bunifuBtnSalir.OnPressedState.BorderRadius = 10;
            this.bunifuBtnSalir.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.OnPressedState.BorderThickness = 2;
            this.bunifuBtnSalir.OnPressedState.FillColor = System.Drawing.Color.Maroon;
            this.bunifuBtnSalir.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnSalir.OnPressedState.IconLeftImage = null;
            this.bunifuBtnSalir.OnPressedState.IconRightImage = null;
            this.bunifuBtnSalir.Size = new System.Drawing.Size(125, 43);
            this.bunifuBtnSalir.TabIndex = 33;
            this.bunifuBtnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuBtnSalir.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuBtnSalir.TextMarginLeft = 0;
            this.bunifuBtnSalir.TextPadding = new System.Windows.Forms.Padding(0);
            this.bunifuBtnSalir.UseDefaultRadiusAndThickness = true;
            this.bunifuBtnSalir.Click += new System.EventHandler(this.bunifuBtnSalir_Click);
            // 
            // Actividad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1363, 769);
            this.Controls.Add(this.bunifuSeparator2);
            this.Controls.Add(this.bunifuLabel1);
            this.Controls.Add(this.bunifuBtnSalir);
            this.Controls.Add(this.bunifuCards1);
            this.Name = "Actividad";
            this.Text = "Actividad";
            this.Load += new System.EventHandler(this.Actividad_Load);
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuDataGridViewActividad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private Bunifu.UI.WinForms.BunifuDropdown bunifuCbxGruposServicio;
        private Bunifu.UI.WinForms.BunifuDataGridView bunifuDataGridViewActividad;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton2 bunifuBtnBuscar;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLblGrupoServicio;
        private Bunifu.UI.WinForms.BunifuSeparator bunifuSeparator2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton2 bunifuBtnSalir;
    }
}