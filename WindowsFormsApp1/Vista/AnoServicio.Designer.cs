﻿namespace WindowsFormsApp1.Vista
{
    partial class AnoServicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnoServicio));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderEdges borderEdges2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderEdges();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuCards2 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuSeparator2 = new Bunifu.UI.WinForms.BunifuSeparator();
            this.bunifuSeparator1 = new Bunifu.UI.WinForms.BunifuSeparator();
            this.bunifuSeparator3 = new Bunifu.UI.WinForms.BunifuSeparator();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel3 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuBtnSalir = new Bunifu.UI.WinForms.BunifuButton.BunifuButton2();
            this.bunifuBtnCrearAnoServicio = new Bunifu.UI.WinForms.BunifuButton.BunifuButton2();
            this.bunifuCards1.SuspendLayout();
            this.bunifuCards2.SuspendLayout();
            this.bunifuCards3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.DodgerBlue;
            this.bunifuCards1.Controls.Add(this.bunifuBtnSalir);
            this.bunifuCards1.Controls.Add(this.bunifuLabel1);
            this.bunifuCards1.Controls.Add(this.bunifuSeparator2);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(787, 50);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(519, 690);
            this.bunifuCards1.TabIndex = 0;
            // 
            // bunifuCards2
            // 
            this.bunifuCards2.BackColor = System.Drawing.Color.White;
            this.bunifuCards2.BorderRadius = 5;
            this.bunifuCards2.BottomSahddow = true;
            this.bunifuCards2.color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bunifuCards2.Controls.Add(this.bunifuBtnCrearAnoServicio);
            this.bunifuCards2.Controls.Add(this.bunifuLabel2);
            this.bunifuCards2.Controls.Add(this.bunifuSeparator1);
            this.bunifuCards2.LeftSahddow = false;
            this.bunifuCards2.Location = new System.Drawing.Point(59, 50);
            this.bunifuCards2.Name = "bunifuCards2";
            this.bunifuCards2.RightSahddow = true;
            this.bunifuCards2.ShadowDepth = 20;
            this.bunifuCards2.Size = new System.Drawing.Size(676, 338);
            this.bunifuCards2.TabIndex = 1;
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.BackColor = System.Drawing.Color.White;
            this.bunifuCards3.BorderRadius = 5;
            this.bunifuCards3.BottomSahddow = true;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuCards3.Controls.Add(this.bunifuLabel3);
            this.bunifuCards3.Controls.Add(this.bunifuSeparator3);
            this.bunifuCards3.LeftSahddow = false;
            this.bunifuCards3.Location = new System.Drawing.Point(59, 402);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = true;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(676, 338);
            this.bunifuCards3.TabIndex = 2;
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuSeparator2.BackgroundImage")));
            this.bunifuSeparator2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuSeparator2.DashCap = Bunifu.UI.WinForms.BunifuSeparator.CapStyles.Flat;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.Silver;
            this.bunifuSeparator2.LineStyle = Bunifu.UI.WinForms.BunifuSeparator.LineStyles.Solid;
            this.bunifuSeparator2.LineThickness = 1;
            this.bunifuSeparator2.Location = new System.Drawing.Point(18, 38);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Orientation = Bunifu.UI.WinForms.BunifuSeparator.LineOrientation.Horizontal;
            this.bunifuSeparator2.Size = new System.Drawing.Size(493, 29);
            this.bunifuSeparator2.TabIndex = 32;
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuSeparator1.BackgroundImage")));
            this.bunifuSeparator1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuSeparator1.DashCap = Bunifu.UI.WinForms.BunifuSeparator.CapStyles.Flat;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.Silver;
            this.bunifuSeparator1.LineStyle = Bunifu.UI.WinForms.BunifuSeparator.LineStyles.Solid;
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(14, 38);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Orientation = Bunifu.UI.WinForms.BunifuSeparator.LineOrientation.Horizontal;
            this.bunifuSeparator1.Size = new System.Drawing.Size(644, 29);
            this.bunifuSeparator1.TabIndex = 33;
            // 
            // bunifuSeparator3
            // 
            this.bunifuSeparator3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuSeparator3.BackgroundImage")));
            this.bunifuSeparator3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuSeparator3.DashCap = Bunifu.UI.WinForms.BunifuSeparator.CapStyles.Flat;
            this.bunifuSeparator3.LineColor = System.Drawing.Color.Silver;
            this.bunifuSeparator3.LineStyle = Bunifu.UI.WinForms.BunifuSeparator.LineStyles.Solid;
            this.bunifuSeparator3.LineThickness = 1;
            this.bunifuSeparator3.Location = new System.Drawing.Point(14, 42);
            this.bunifuSeparator3.Name = "bunifuSeparator3";
            this.bunifuSeparator3.Orientation = Bunifu.UI.WinForms.BunifuSeparator.LineOrientation.Horizontal;
            this.bunifuSeparator3.Size = new System.Drawing.Size(644, 29);
            this.bunifuSeparator3.TabIndex = 34;
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.AllowParentOverrides = false;
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel1.CursorType = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.ForeColor = System.Drawing.Color.SteelBlue;
            this.bunifuLabel1.Location = new System.Drawing.Point(18, 24);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(94, 19);
            this.bunifuLabel1.TabIndex = 33;
            this.bunifuLabel1.Text = "Información";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.AllowParentOverrides = false;
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel2.CursorType = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel2.ForeColor = System.Drawing.Color.SteelBlue;
            this.bunifuLabel2.Location = new System.Drawing.Point(14, 24);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(172, 19);
            this.bunifuLabel2.TabIndex = 33;
            this.bunifuLabel2.Text = "Creación Año Servicio";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel3
            // 
            this.bunifuLabel3.AllowParentOverrides = false;
            this.bunifuLabel3.AutoEllipsis = false;
            this.bunifuLabel3.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel3.CursorType = System.Windows.Forms.Cursors.Default;
            this.bunifuLabel3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel3.ForeColor = System.Drawing.Color.SteelBlue;
            this.bunifuLabel3.Location = new System.Drawing.Point(14, 27);
            this.bunifuLabel3.Name = "bunifuLabel3";
            this.bunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel3.Size = new System.Drawing.Size(289, 19);
            this.bunifuLabel3.TabIndex = 34;
            this.bunifuLabel3.Text = "Creación Año Servicio Por Publicador";
            this.bunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuBtnSalir
            // 
            this.bunifuBtnSalir.AllowAnimations = true;
            this.bunifuBtnSalir.AllowMouseEffects = true;
            this.bunifuBtnSalir.AllowToggling = false;
            this.bunifuBtnSalir.AnimationSpeed = 200;
            this.bunifuBtnSalir.AutoGenerateColors = false;
            this.bunifuBtnSalir.AutoRoundBorders = false;
            this.bunifuBtnSalir.AutoSizeLeftIcon = true;
            this.bunifuBtnSalir.AutoSizeRightIcon = true;
            this.bunifuBtnSalir.BackColor = System.Drawing.Color.Transparent;
            this.bunifuBtnSalir.BackColor1 = System.Drawing.Color.White;
            this.bunifuBtnSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnSalir.BackgroundImage")));
            this.bunifuBtnSalir.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.ButtonText = "     Salir";
            this.bunifuBtnSalir.ButtonTextMarginLeft = 0;
            this.bunifuBtnSalir.ColorContrastOnClick = 45;
            this.bunifuBtnSalir.ColorContrastOnHover = 45;
            this.bunifuBtnSalir.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.bunifuBtnSalir.CustomizableEdges = borderEdges1;
            this.bunifuBtnSalir.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuBtnSalir.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuBtnSalir.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuBtnSalir.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuBtnSalir.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.ButtonStates.Pressed;
            this.bunifuBtnSalir.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuBtnSalir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuBtnSalir.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuBtnSalir.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuBtnSalir.IconMarginLeft = 11;
            this.bunifuBtnSalir.IconPadding = 10;
            this.bunifuBtnSalir.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuBtnSalir.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuBtnSalir.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuBtnSalir.IconSize = 25;
            this.bunifuBtnSalir.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.IdleBorderRadius = 10;
            this.bunifuBtnSalir.IdleBorderThickness = 2;
            this.bunifuBtnSalir.IdleFillColor = System.Drawing.Color.White;
            this.bunifuBtnSalir.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnSalir.IdleIconLeftImage")));
            this.bunifuBtnSalir.IdleIconRightImage = null;
            this.bunifuBtnSalir.IndicateFocus = false;
            this.bunifuBtnSalir.Location = new System.Drawing.Point(371, 633);
            this.bunifuBtnSalir.Name = "bunifuBtnSalir";
            this.bunifuBtnSalir.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuBtnSalir.OnDisabledState.BorderRadius = 10;
            this.bunifuBtnSalir.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.OnDisabledState.BorderThickness = 2;
            this.bunifuBtnSalir.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuBtnSalir.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuBtnSalir.OnDisabledState.IconLeftImage = null;
            this.bunifuBtnSalir.OnDisabledState.IconRightImage = null;
            this.bunifuBtnSalir.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.onHoverState.BorderRadius = 10;
            this.bunifuBtnSalir.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.onHoverState.BorderThickness = 2;
            this.bunifuBtnSalir.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnSalir.onHoverState.IconLeftImage = null;
            this.bunifuBtnSalir.onHoverState.IconRightImage = null;
            this.bunifuBtnSalir.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.OnIdleState.BorderRadius = 10;
            this.bunifuBtnSalir.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.OnIdleState.BorderThickness = 2;
            this.bunifuBtnSalir.OnIdleState.FillColor = System.Drawing.Color.White;
            this.bunifuBtnSalir.OnIdleState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bunifuBtnSalir.OnIdleState.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnSalir.OnIdleState.IconLeftImage")));
            this.bunifuBtnSalir.OnIdleState.IconRightImage = null;
            this.bunifuBtnSalir.OnPressedState.BorderColor = System.Drawing.Color.Maroon;
            this.bunifuBtnSalir.OnPressedState.BorderRadius = 10;
            this.bunifuBtnSalir.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnSalir.OnPressedState.BorderThickness = 2;
            this.bunifuBtnSalir.OnPressedState.FillColor = System.Drawing.Color.Maroon;
            this.bunifuBtnSalir.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnSalir.OnPressedState.IconLeftImage = null;
            this.bunifuBtnSalir.OnPressedState.IconRightImage = null;
            this.bunifuBtnSalir.Size = new System.Drawing.Size(125, 43);
            this.bunifuBtnSalir.TabIndex = 34;
            this.bunifuBtnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuBtnSalir.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuBtnSalir.TextMarginLeft = 0;
            this.bunifuBtnSalir.TextPadding = new System.Windows.Forms.Padding(0);
            this.bunifuBtnSalir.UseDefaultRadiusAndThickness = true;
            this.bunifuBtnSalir.Click += new System.EventHandler(this.bunifuBtnSalir_Click);
            // 
            // bunifuBtnCrearAnoServicio
            // 
            this.bunifuBtnCrearAnoServicio.AllowAnimations = true;
            this.bunifuBtnCrearAnoServicio.AllowMouseEffects = true;
            this.bunifuBtnCrearAnoServicio.AllowToggling = false;
            this.bunifuBtnCrearAnoServicio.AnimationSpeed = 200;
            this.bunifuBtnCrearAnoServicio.AutoGenerateColors = false;
            this.bunifuBtnCrearAnoServicio.AutoRoundBorders = false;
            this.bunifuBtnCrearAnoServicio.AutoSizeLeftIcon = true;
            this.bunifuBtnCrearAnoServicio.AutoSizeRightIcon = true;
            this.bunifuBtnCrearAnoServicio.BackColor = System.Drawing.Color.Transparent;
            this.bunifuBtnCrearAnoServicio.BackColor1 = System.Drawing.Color.DodgerBlue;
            this.bunifuBtnCrearAnoServicio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuBtnCrearAnoServicio.BackgroundImage")));
            this.bunifuBtnCrearAnoServicio.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnCrearAnoServicio.ButtonText = "Ejecutar Creación";
            this.bunifuBtnCrearAnoServicio.ButtonTextMarginLeft = 0;
            this.bunifuBtnCrearAnoServicio.ColorContrastOnClick = 45;
            this.bunifuBtnCrearAnoServicio.ColorContrastOnHover = 45;
            this.bunifuBtnCrearAnoServicio.Cursor = System.Windows.Forms.Cursors.Default;
            borderEdges2.BottomLeft = true;
            borderEdges2.BottomRight = true;
            borderEdges2.TopLeft = true;
            borderEdges2.TopRight = true;
            this.bunifuBtnCrearAnoServicio.CustomizableEdges = borderEdges2;
            this.bunifuBtnCrearAnoServicio.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuBtnCrearAnoServicio.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuBtnCrearAnoServicio.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuBtnCrearAnoServicio.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuBtnCrearAnoServicio.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.ButtonStates.Pressed;
            this.bunifuBtnCrearAnoServicio.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bunifuBtnCrearAnoServicio.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnCrearAnoServicio.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuBtnCrearAnoServicio.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuBtnCrearAnoServicio.IconLeftPadding = new System.Windows.Forms.Padding(11, 3, 3, 3);
            this.bunifuBtnCrearAnoServicio.IconMarginLeft = 11;
            this.bunifuBtnCrearAnoServicio.IconPadding = 10;
            this.bunifuBtnCrearAnoServicio.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bunifuBtnCrearAnoServicio.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuBtnCrearAnoServicio.IconRightPadding = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.bunifuBtnCrearAnoServicio.IconSize = 25;
            this.bunifuBtnCrearAnoServicio.IdleBorderColor = System.Drawing.Color.DodgerBlue;
            this.bunifuBtnCrearAnoServicio.IdleBorderRadius = 1;
            this.bunifuBtnCrearAnoServicio.IdleBorderThickness = 1;
            this.bunifuBtnCrearAnoServicio.IdleFillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuBtnCrearAnoServicio.IdleIconLeftImage = null;
            this.bunifuBtnCrearAnoServicio.IdleIconRightImage = null;
            this.bunifuBtnCrearAnoServicio.IndicateFocus = false;
            this.bunifuBtnCrearAnoServicio.Location = new System.Drawing.Point(74, 110);
            this.bunifuBtnCrearAnoServicio.Name = "bunifuBtnCrearAnoServicio";
            this.bunifuBtnCrearAnoServicio.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuBtnCrearAnoServicio.OnDisabledState.BorderRadius = 1;
            this.bunifuBtnCrearAnoServicio.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnCrearAnoServicio.OnDisabledState.BorderThickness = 1;
            this.bunifuBtnCrearAnoServicio.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuBtnCrearAnoServicio.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuBtnCrearAnoServicio.OnDisabledState.IconLeftImage = null;
            this.bunifuBtnCrearAnoServicio.OnDisabledState.IconRightImage = null;
            this.bunifuBtnCrearAnoServicio.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuBtnCrearAnoServicio.onHoverState.BorderRadius = 1;
            this.bunifuBtnCrearAnoServicio.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnCrearAnoServicio.onHoverState.BorderThickness = 1;
            this.bunifuBtnCrearAnoServicio.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuBtnCrearAnoServicio.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnCrearAnoServicio.onHoverState.IconLeftImage = null;
            this.bunifuBtnCrearAnoServicio.onHoverState.IconRightImage = null;
            this.bunifuBtnCrearAnoServicio.OnIdleState.BorderColor = System.Drawing.Color.DodgerBlue;
            this.bunifuBtnCrearAnoServicio.OnIdleState.BorderRadius = 1;
            this.bunifuBtnCrearAnoServicio.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnCrearAnoServicio.OnIdleState.BorderThickness = 1;
            this.bunifuBtnCrearAnoServicio.OnIdleState.FillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuBtnCrearAnoServicio.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnCrearAnoServicio.OnIdleState.IconLeftImage = null;
            this.bunifuBtnCrearAnoServicio.OnIdleState.IconRightImage = null;
            this.bunifuBtnCrearAnoServicio.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuBtnCrearAnoServicio.OnPressedState.BorderRadius = 1;
            this.bunifuBtnCrearAnoServicio.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton2.BorderStyles.Solid;
            this.bunifuBtnCrearAnoServicio.OnPressedState.BorderThickness = 1;
            this.bunifuBtnCrearAnoServicio.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuBtnCrearAnoServicio.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuBtnCrearAnoServicio.OnPressedState.IconLeftImage = null;
            this.bunifuBtnCrearAnoServicio.OnPressedState.IconRightImage = null;
            this.bunifuBtnCrearAnoServicio.Size = new System.Drawing.Size(151, 50);
            this.bunifuBtnCrearAnoServicio.TabIndex = 34;
            this.bunifuBtnCrearAnoServicio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuBtnCrearAnoServicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuBtnCrearAnoServicio.TextMarginLeft = 0;
            this.bunifuBtnCrearAnoServicio.TextPadding = new System.Windows.Forms.Padding(0);
            this.bunifuBtnCrearAnoServicio.UseDefaultRadiusAndThickness = true;
            this.bunifuBtnCrearAnoServicio.Click += new System.EventHandler(this.bunifuBtnCrearAnoServicio_Click);
            // 
            // AnoServicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1363, 769);
            this.Controls.Add(this.bunifuCards3);
            this.Controls.Add(this.bunifuCards2);
            this.Controls.Add(this.bunifuCards1);
            this.Name = "AnoServicio";
            this.Text = "AnoServicio";
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            this.bunifuCards2.ResumeLayout(false);
            this.bunifuCards2.PerformLayout();
            this.bunifuCards3.ResumeLayout(false);
            this.bunifuCards3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private Bunifu.Framework.UI.BunifuCards bunifuCards2;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private Bunifu.UI.WinForms.BunifuSeparator bunifuSeparator2;
        private Bunifu.UI.WinForms.BunifuSeparator bunifuSeparator1;
        private Bunifu.UI.WinForms.BunifuSeparator bunifuSeparator3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel3;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton2 bunifuBtnSalir;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton2 bunifuBtnCrearAnoServicio;
    }
}