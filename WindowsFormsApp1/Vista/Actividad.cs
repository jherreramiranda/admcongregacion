﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Controlador;

namespace WindowsFormsApp1.Vista
{
    public partial class Actividad : Form
    {
        public Actividad()
        {
            InitializeComponent();
        }

        private void bunifuBtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Actividad_Load(object sender, EventArgs e)
        {
            GenericosController controlesGenericos = new GenericosController();
            var listaGruposCogregacion = controlesGenericos.obtenerGruposCongregacion();

            this.bunifuCbxGruposServicio.DataSource = listaGruposCogregacion;
            this.bunifuCbxGruposServicio.ValueMember = "IdGrupo";
            this.bunifuCbxGruposServicio.DisplayMember = "NombreGrupo";

            AnoServicioController anoServicioFull = new AnoServicioController();
            bunifuDataGridViewActividad.DataSource = anoServicioFull.cargarDataGridActividad();

        }
    }
}
